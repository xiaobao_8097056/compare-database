var ReqUtils = {
    req : function (url, type, data) {
        if (type === '') { type = 'GET' }
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: url,
                data: data,
                type: type,
                contentType: "application/json",
                header: {
                    'Content-Type': 'application/json;charset=UTF-8' // 默认值
                },
                success: function (res) {
                    console.log(res);
                    resolve(res);
                },
                fail: function (error) {
                    reject(error);
                },
                complete: function (error) {
                    reject(error);
                }
            })
         });
    }
}