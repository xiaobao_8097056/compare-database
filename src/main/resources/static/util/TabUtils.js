var TabUtils = {
    /**
     *  在内容主体区域添加动态tab页
     *  ============================================================================================
     *  入参：
     *      tab_id          【全局唯一主键】
     *      tab_title       【tab标签名称】
     *      url             【静态资源请求地址】
     *      custom_obj      【自定义对象】
     *  ============================================================================================
     */
    addContentTab: function (tab_id, tab_title, url, custom_obj) {

        let lay_id = 'tab-'+tab_id;
        let titElem = $('li[lay-id="'+lay_id+'"]');
        if (titElem.length > 0) {
            layui.element.tabChange('database-tabs', lay_id);
            return;
        }
        $.get(url, function (result) {
            layui.element.tabAdd('database-tabs', {
                id: lay_id,
                title: tab_title,
                content: result
            });
            if (custom_obj) {
                $('li[lay-id="'+lay_id+'"]')[0]['tabCustomObj'] = custom_obj;
            }
            layui.element.tabChange('database-tabs', lay_id);
            // 增加Tab页点击监听事件
            var titElem = $('li[lay-id="'+lay_id+'"]');
            titElem.bind('tabClick');
            titElem.click(function (event) {
                if (event.target.className != 'layui-icon layui-unselect layui-tab-close') {
                    layui.element.tabChange('databases-tabs', event.currentTarget.getAttribute('lay-id'));
                    titElem.trigger('tabClick');
                }
            });

        });
    }
}