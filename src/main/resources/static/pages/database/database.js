layui.element.on('tab(database-tabs)', function (data) {
    let baseId = $('#tabTitles')[0].children[data.index].tabCustomObj;
    let baseInfoId = 'databaseInfo-' + baseId;
    let sourceDatabasesId = 'sourceDatabases-' + baseId;
    let targetDatabasesId = 'targetDatabases-' + baseId;

    let $sourceDatabases = $('#sourceDatabases');
    let $targetDatabases = $('#targetDatabases');
    let $compareTable = $('#compareTable');
    let compareTableId = baseId + 'compare';
    let table = layui.table;

    $('#databaseInfo').attr('id', baseInfoId);
    $sourceDatabases.attr('id', sourceDatabasesId);
    $targetDatabases.attr('id', targetDatabasesId);
    $compareTable.attr('id', compareTableId);

    $sourceDatabases.attr('lay-filter', sourceDatabasesId);
    $targetDatabases.attr('lay-filter', targetDatabasesId);


    let baseInfo = {
        render: function (sourceDatabasesId, targetDatabasesId, baseId) {
            table.render({
                id: sourceDatabasesId
                , elem: '#' + sourceDatabasesId
                , height: 'full-210'
                , url: '/dbInfo?type=SOURCE&id=' + baseId //数据接口
                , cols: [[ //表头
                    {type: 'checkbox'}
                    , {title: '<b>数据库名</b>', field: 'name', event : 'tick', templet: '#sourceColor'},
                ]],
                response: {
                    statusName: 'code' //数据状态的字段名称，默认：code
                    , statusCode: 200 //成功的状态码，默认：0
                    , dataName: 'data' //数据列表的字段名称，默认：data
                }
            });

            table.render({
                id: targetDatabasesId
                , elem: '#' + targetDatabasesId
                , height: 'full-210'
                , url: '/dbInfo?type=TARGET&id=' + baseId //数据接口
                , cols: [[ //表头
                    {type: 'checkbox'}
                    , {title: '<b>数据库名</b>', field: 'name', event : 'tick', templet: '#targetColor'},
                ]],
                response: {
                    statusName: 'code' //数据状态的字段名称，默认：code
                    , statusCode: 200 //成功的状态码，默认：0
                    , dataName: 'data' //数据列表的字段名称，默认：data
                }
            });
            $('.layui-table-header').find('.laytable-cell-checkbox').remove();
        }
    };
    baseInfo.render(sourceDatabasesId, targetDatabasesId, baseId);

    let active = {
        getCheckData: function (tableId) { //获取选中数据
            let checkStatus = table.checkStatus(tableId)
                , data = checkStatus.data;
            return data;
        }
        , getCheckLength: function (tableId) { //获取选中数目
            let checkStatus = table.checkStatus(tableId)
                , data = checkStatus.data;
            return data.length;
        }
        , isAll: function (tableId) { //验证是否全选
            let checkStatus = table.checkStatus(tableId);
            return checkStatus.isAll;
        }
    };
    table.on('checkbox(' + sourceDatabasesId + ')', function (obj) {
        if (active.getCheckLength(sourceDatabasesId) > 1) {
            $(this).attr('class', 'layui-unselect layui-form-checkbox');
            layer.msg('不能选择多个库');
            return;
        }
    });
    table.on('checkbox(' + targetDatabasesId + ')', function (obj) {
        if (active.getCheckLength(targetDatabasesId) > 1) {
            $(this).attr('class', 'layui-unselect layui-form-checkbox');
            layer.msg('不能选择多个库');
            return;
        }
    });
    /*let sourceCheckLength = 0;
    let targetCheckLength = 0;
    table.on('tool(' + sourceDatabasesId + ')', function(obj){
        let $tr = $(this).closest('tr');
        let $box = $tr.find('i').parent();
        if (active.getCheckLength(sourceDatabasesId) >= 1) {
            $box.removeClass('layui-form-checked');
            layer.msg('不能选择多个库');
            return;
        }
        if(obj.event === 'tick'){
            if ($box.hasClass('layui-form-checked')) {
                $box.removeClass('layui-form-checked');
                sourceCheckLength--;
            }else{
                $box.addClass('layui-form-checked');
                sourceCheckLength++;
            }
            layui.element.render();
        }
    });
    table.on('tool(' + targetDatabasesId + ')', function(obj){
        let $tr = $(this).closest('tr');
        let $box = $tr.find('i').parent();
        if (active.getCheckLength(targetDatabasesId) >= 1) {
            $box.removeClass('layui-form-checked');
            layer.msg('不能选择多个库');
            return;
        }

        if(obj.event === 'tick'){
            if ($box.hasClass('layui-form-checked')) {
                $box.removeClass('layui-form-checked');
                targetCheckLength--;
            }else{
                $box.addClass('layui-form-checked');
                targetCheckLength++;
            }
            layui.element.render();
        }
    });*/
    $('#'+compareTableId).click(function () {
        let sourceCheckData = active.getCheckData(sourceDatabasesId);
        let targetCheckData = active.getCheckData(targetDatabasesId);
        if (sourceCheckData.length === 0) {
            layer.msg("请选择源库");
            return;
        }
        if (targetCheckData.length === 0) {
            layer.msg("请选择目标库");
            return;
        }
        let sourceName = active.getCheckData(sourceDatabasesId)[0].name;
        let targetName = active.getCheckData(targetDatabasesId)[0].name;
        let compareName = sourceName + '-' + targetName;
        let id = 'table-' + baseId + '-' + compareName;
        let compareTableInfo = {
            baseId: baseId,
            sourceName: sourceName,
            targetName: targetName,
            compareName: compareName,
            compareTableInfoId: id
        }
        if (id) {
            TabUtils.addContentTab(id, 'table-' + compareName, "/pages/tables/tableInfo.html", compareTableInfo);
            layui.element.render('nav', 'database-list');
        }
    });
})

