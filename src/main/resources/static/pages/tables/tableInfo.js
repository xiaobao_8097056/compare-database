layui.element.on('tab(database-tabs)', function (data) {
    let compareTableInfo = $('#tabTitles')[0].children[data.index].tabCustomObj;
    let baseId = compareTableInfo.baseId;
    let compareName = compareTableInfo.compareName;
    let tableInfoId = compareTableInfo.compareTableInfoId;
    let $compareColumn = $('#compareColumn');
    let compareColumnId = baseId + 'compare'+compareName;
    let table = layui.table;

    let sourceTableId = 'source-' + baseId + '-' + compareName;
    let targetTableId = 'target-' + baseId + '-' + compareName;
    let $sourceTable = $('#sourceTables');
    let $targetTable = $('#targetTables');


    $('#tableInfo').attr('id', tableInfoId);
    $sourceTable.attr('id', sourceTableId);
    $targetTable.attr('id', targetTableId);
    $compareColumn.attr('id', compareColumnId);
    $sourceTable.attr('lay-filter', sourceTableId);
    $targetTable.attr('lay-filter', targetTableId);


    let tableInfo = {
        render: function (sourceTableId, targetTableId, baseId) {
            table.render({
                id: sourceTableId
                , elem: '#' + sourceTableId
                , height: 'full-210'
                , url: `/dbTableInfo?type=SOURCE&baseId=${baseId}&sourceName=${compareTableInfo.sourceName}
                        &targetName=${compareTableInfo.targetName}` //数据接口
                , cols: [[ //表头
                    {type: 'checkbox'}
                    , {title: '<b>table name</b>', field: 'name', templet: '#sourceTableColor'}
                ]],
                response: {
                    statusName: 'code' //数据状态的字段名称，默认：code
                    , statusCode: 200 //成功的状态码，默认：0
                    , dataName: 'data' //数据列表的字段名称，默认：data
                }
            });

            table.render({
                id: targetTableId
                , elem: '#' + targetTableId
                , height: 'full-210'
                , url: `/dbTableInfo?type=TARGET&baseId=${baseId}&sourceName=${compareTableInfo.sourceName}
                        &targetName=${compareTableInfo.targetName}` //数据接口
                , cols: [[ //表头
                    {type: 'checkbox'}
                    , {title: '<b>table name</b>', field: 'name', templet: '#targetTableColor'}
                ]],
                response: {
                    statusName: 'code' //数据状态的字段名称，默认：code
                    , statusCode: 200 //成功的状态码，默认：0
                    , dataName: 'data' //数据列表的字段名称，默认：data
                }
            });
            $('.layui-table-header').find('.laytable-cell-checkbox').remove();
        }
    };
    let active = {
        getCheckData: function (tableId) { //获取选中数据
            let checkStatus = table.checkStatus(tableId)
                , data = checkStatus.data;
            return data;
        }
        , getCheckLength: function (tableId) { //获取选中数目
            let checkStatus = table.checkStatus(tableId)
                , data = checkStatus.data;
            return data.length;
        }
        , isAll: function (tableId) { //验证是否全选
            let checkStatus = table.checkStatus(tableId);
            return checkStatus.isAll;
        }
    };

    tableInfo.render(sourceTableId, targetTableId, baseId);
    table.on('checkbox(' + sourceTableId + ')', function (obj) {
        if (active.getCheckLength(sourceTableId) > 1) {
            $(this).attr('class', 'layui-unselect layui-form-checkbox');
            layer.msg('不能选择多张表');
            return;
        }
    });
    table.on('checkbox(' + targetTableId + ')', function (obj) {
        if (active.getCheckLength(targetTableId) > 1) {
            $(this).attr('class', 'layui-unselect layui-form-checkbox');
            layer.msg('不能选择多张表');
            return;
        }
    });

    $('#'+compareColumnId).click(function () {
        alert(123423);
        let sourceCheckData = active.getCheckData(sourceTableId);
        let targetCheckData = active.getCheckData(targetTableId);
        if (sourceCheckData.length === 0) {
            layer.msg("请选择源库中的表");
            return;
        }
        if (targetCheckData.length === 0) {
            layer.msg("请选择目标库中的表");
            return;
        }
        let sourceTableName = active.getCheckData(sourceTableId)[0].name;
        let targetTableName = active.getCheckData(targetTableId)[0].name;
        let compareName = sourceTableName + '-' + targetTableName;
        let id = 'column-' + baseId + '-' + compareName;
        let compareColumnInfo = {
            baseId: baseId,
            sourceName : compareTableInfo.sourceName,
            targetName : compareTableInfo.targetName,
            sourceTableName: sourceTableName,
            targetTableName: targetTableName,
            compareName: compareName,
            compareTableInfoId: id
        }
        console.log(compareColumnInfo);
        if (id) {
            TabUtils.addContentTab(id, 'column-' + compareName, "/pages/column/tableColumn.html", compareColumnInfo);
            layui.element.render('nav', 'database-list');
        }
    });
})

