$(function () {
    let dbs_fn = {
        showDbs: function () {
            ReqUtils.req('/dbs', 'get', {}).then(res=>{
                if (res.code === 200) {
                    if (!res.data || res.data.length === 0) {
                        window.location.href = "../../index.html";
                    }
                    let bases = '';
                    layui.each(res.data, function (index, item) {
                        bases += '<dd>' +
                            '<a id="' + item[1] + '" url="/pages/database/database.html?' + item[1] + '" style="cursor: pointer">' + item[0] + '</a>' +
                            '</dd>';
                    });
                    $('#catalog').html(bases);
                    layui.element.render('nav', 'database-list');
                }else {
                    layer.msg('显示数据库错误');
                }
            })
        }
    };
    dbs_fn.showDbs();
    // 监听左侧导航区域
    layui.element.on('nav(database-list)', function (data) {
        let id = data.attr("id");
        if (id) {
            TabUtils.addContentTab(id, data.text(), "/pages/database/database.html", id);
            layui.element.render('nav', 'database-list');
        }
    });
})