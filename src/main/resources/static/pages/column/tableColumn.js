layui.element.on('tab(database-tabs)', function (data) {
    let compareColumnInfo = $('#tabTitles')[0].children[data.index].tabCustomObj;
    /*let compareColumnInfo = {
        baseId: baseId,
        sourceName : compareTableInfo.sourceName,
        targetName : compareTableInfo.targetName,
        sourceTableName: sourceTableName,
        targetTableName: targetTableName,
        compareName: compareName,
        compareTableInfoId: id
    }*/
    console.log(compareColumnInfo);
    let baseId = compareColumnInfo.baseId;

    let compareName = compareColumnInfo.compareName;
    let columnInfoId = compareColumnInfo.compareTableInfoId;

    let sourceColumnId = 'source-' + baseId + '-' + compareColumnInfo.sourceName + compareColumnInfo.targetName + compareName;
    let targetColumnId = 'target-' + baseId + '-' + compareColumnInfo.sourceName + compareColumnInfo.targetName + compareName;
    let $sourceColumn = $('#sourceColumns');
    let $targetColumn = $('#targetColumns');
    let table = layui.table;

    $('#tableInfo').attr('id', columnInfoId);
    $sourceColumn.attr('id', sourceColumnId);
    $targetColumn.attr('id', targetColumnId);
    $sourceColumn.attr('lay-filter', sourceColumnId);
    $targetColumn.attr('lay-filter', targetColumnId);

    let columnInfo = {
        render: function (sourceColumnId, targetColumnId, baseId) {
            table.render({
                id: sourceColumnId
                , elem: '#' + sourceColumnId
                , height: 'full-210'
                , url: `/dbTableColumnInfo?type=SOURCE&baseId=${baseId}&sourceName=${compareColumnInfo.sourceName}
                        &targetName=${compareColumnInfo.targetName}&sourceTableName=${compareColumnInfo.sourceTableName}
                        &targetTableName=${compareColumnInfo.targetTableName}` //数据接口
                , cols: [[ //表头
                    {type: 'checkbox'}
                    , {title: '<b>table name</b>', field: 'name', templet: '#sourceColumnColor'}
                ]],
                response: {
                    statusName: 'code' //数据状态的字段名称，默认：code
                    , statusCode: 200 //成功的状态码，默认：0
                    , dataName: 'data' //数据列表的字段名称，默认：data
                }
            });

            table.render({
                id: targetColumnId
                , elem: '#' + targetColumnId
                , height: 'full-210'
                , url: `/dbTableColumnInfo?type=TARGET&baseId=${baseId}&sourceName=${compareColumnInfo.sourceName}
                        &targetName=${compareColumnInfo.targetName}&sourceTableName=${compareColumnInfo.sourceTableName}
                        &targetTableName=${compareColumnInfo.targetTableName}` //数据接口
                , cols: [[ //表头
                    {type: 'checkbox'}
                    , {title: '<b>table name</b>', field: 'name', templet: '#targetColumnColor'}
                ]],
                response: {
                    statusName: 'code' //数据状态的字段名称，默认：code
                    , statusCode: 200 //成功的状态码，默认：0
                    , dataName: 'data' //数据列表的字段名称，默认：data
                }
            });
            $('.layui-table-header').find('.laytable-cell-checkbox').remove();
        }
    };
    columnInfo.render(sourceColumnId, targetColumnId, baseId);

    let active = {
        getCheckData: function (columnId) { //获取选中数据
            let checkStatus = table.checkStatus(columnId)
                , data = checkStatus.data;
            return data;
        }
        , getCheckLength: function (columnId) { //获取选中数目
            let checkStatus = table.checkStatus(columnId)
                , data = checkStatus.data;
            return data.length;
        }
        , isAll: function (columnId) { //验证是否全选
            let checkStatus = table.checkStatus(columnId);
            return checkStatus.isAll;
        }
    };
    table.on('checkbox(' + targetColumnId + ')', function (obj) {
        if (active.getCheckLength(targetColumnId) > 1) {
            $(this).attr('class', 'layui-unselect layui-form-checkbox');
            layer.msg('不能选择多张表');
            return;
        }
    });
    table.on('checkbox(' + sourceColumnId + ')', function (obj) {
        if (active.getCheckLength(sourceColumnId) > 1) {
            $(this).attr('class', 'layui-unselect layui-form-checkbox');
            layer.msg('不能选择多张表');
            return;
        }
    });

})

