package com.dosimple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompareDatebaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompareDatebaseApplication.class, args);
	}
}
