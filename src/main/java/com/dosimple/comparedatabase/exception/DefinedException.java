package com.dosimple.comparedatabase.exception;

/**
 * @auther: baolw
 * @date: 2018/6/20 0020 上午 9:19
 */
public class DefinedException extends RuntimeException {
    public DefinedException(String message) {
        super(message);
    }
}
