package com.dosimple.comparedatabase.common;

import lombok.Data;

/**
 * @auther: baolw
 * @date: 2018/6/19 0019 下午 5:08
 */
@Data
public class CommonResult<T> {
    private int code;

    private String msg;

    private T data;

    public CommonResult(int code, T data) {
        this.data = data;
        this.code = code;
    }

    public CommonResult(int code, String msg) {
        this.msg = msg;
        this.code = code;
    }

    public CommonResult() {

    }

    public CommonResult(T data, int code) {
        this.data = data;
        this.code = code;
    }
}
