package com.dosimple.comparedatabase.util;

import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.text.Font;
/**
 * @auther: baolw
 * @date: 2018/6/22 0022 下午 4:11
 */
public class FontsProviderUtils extends XMLWorkerFontProvider {
    public FontsProviderUtils(){
        super(null, null);
    }

    @Override
    public Font getFont(final String fontname, String encoding, float size, final int style) {
        String fntname = fontname;
        if (fntname == null) {
            fntname = "宋体";//windows下
            //fntname = "fontFile/simsun.ttf";//linux系统下
        }
        if (size == 0) {
            size = 4;
        }
        return super.getFont(fntname, encoding, size, style);
    }
}
