package com.dosimple.comparedatabase.dto;

import lombok.Data;

@Data
public class TableColumnInfoDto {
    private String columnName;

    private String columnType;

    private int columnSize;
    /**
     * 是否允许使用 NULL。
     */
    private int decimalDigits;

    private String columnDef;

    private String nullAble;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TableColumnInfoDto that = (TableColumnInfoDto) o;

        if (columnSize != that.columnSize) return false;
        if (decimalDigits != that.decimalDigits) return false;
        if (columnName != null ? !columnName.equals(that.columnName) : that.columnName != null) return false;
        if (columnType != null ? !columnType.equals(that.columnType) : that.columnType != null) return false;
        return nullAble != null ? nullAble.equals(that.nullAble) : that.nullAble == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (columnName != null ? columnName.hashCode() : 0);
        result = 31 * result + (columnType != null ? columnType.hashCode() : 0);
        result = 31 * result + columnSize;
        result = 31 * result + decimalDigits;
        result = 31 * result + (nullAble != null ? nullAble.hashCode() : 0);
        return result;
    }
}
