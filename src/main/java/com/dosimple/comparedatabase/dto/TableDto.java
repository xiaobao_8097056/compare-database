package com.dosimple.comparedatabase.dto;

import com.google.common.base.Objects;
import lombok.Data;

import java.util.List;

@Data
public class TableDto {
    private String tableName;

    private List<TableColumnInfoDto> infoDtos;

    @Override
    public int hashCode() {
        return Objects.hashCode(tableName, infoDtos);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final TableDto other = (TableDto) obj;
        if (this.infoDtos.size() != other.infoDtos.size()) {
            return false;
        }
        for (TableColumnInfoDto thisDto: this.infoDtos) {
            boolean flag = false;
            for (TableColumnInfoDto otherDto : other.infoDtos) {
                if (thisDto.equals(otherDto)) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                return false;
            }
        }
        return Objects.equal(this.tableName, other.tableName);
    }
}
