package com.dosimple.comparedatabase.dto;

import lombok.Data;

/**
 * @auther: baolw
 * @date: 2018/6/21 0021 下午 3:35
 */
@Data
public class TableCompareDto {
    private String baseId;

    private String sourceName;

    private String targetName;

    private String type;
}
