package com.dosimple.comparedatabase.dto;

import lombok.Data;

/**
 * @auther: baolw
 * @date: 2018/6/21 0021 上午 9:40
 */
@Data
public class DatabasesDto {
    private String name;

    private String color;

    public DatabasesDto(String name) {
        this.name = name;
    }

    public DatabasesDto() {

    }
}
