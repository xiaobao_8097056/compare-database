package com.dosimple.comparedatabase.dto;

import lombok.Data;

/**
 * @auther: baolw
 * @date: 2018/6/20 0020 上午 9:09
 */
@Data
public class InitCompareDto {
    private String sourceUrl;
    private String sourceUser;
    private String sourcePassword;
    private String targetUrl;
    private String targetUser;
    private String targetPassword;
    private String aliasName;
}
