package com.dosimple.comparedatabase.controller;

import com.dosimple.comparedatabase.util.PDFUtils;
import com.google.common.collect.Maps;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @auther: baolw
 * @date: 2018/6/22 0022 上午 9:57
 */
@RestController
public class TestController {

    public static void main(String[] args) {
        Map<String, String> map = Maps.newHashMap();
        map.put("fill_1", "你好吗");
        map.put("wen_ben", "fas");
        map.put("fill_2", "asdfasdf");

        byte[] pdfBytes = PDFUtils.fillTemplate(map, "/template/remark.pdf");
        PDFUtils.write(pdfBytes, "D:\\remark.pdf");
    }

}
