package com.dosimple.comparedatabase.controller;

import com.dosimple.comparedatabase.common.CommonResult;
import com.dosimple.comparedatabase.util.DBUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * @auther: baolw
 * @date: 2018/6/20 0020 上午 9:54
 */
@Controller
@Slf4j
public class IndexController {

    @GetMapping("dbs")
    @ResponseBody
    public CommonResult<List<String[]>> getDBs() {
        List<String[]> list = new ArrayList<>();
        DBUtils.aliasDbName.forEach((k, v)->{
            String[] str = new String[]{k, v};
            list.add(str);
        });
        return new CommonResult(list, 200);
    }
}
